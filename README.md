Instruction

1. To build the image run this command (This will create a Docker image named "my-nginx-image" using the Dockerfile in your current directory.)
 
 - docker build -t my-nginx-image .

2. To run the image, use this command (This will start a container based on the "my-nginx-image" image, and map port 80 on the container to port 80 on your host machine.)

 - docker run -d -p 80:80 my-nginx-image

3. Go to browser and go to  http:/127.0.0.1/

